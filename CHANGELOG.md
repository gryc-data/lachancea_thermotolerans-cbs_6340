# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.1 (2021-05-17)

### Edited

* Build feature hierarchy.

## v1.0 (2021-05-17)

### Added

* The 8 annotated chromosomes of Lachancea thermotolerans CBS 6340 (source EBI, [GCA_000142805.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000142805.1)).
