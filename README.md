## *Lachancea thermotolerans* CBS 6340

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJNA12499](https://www.ebi.ac.uk/ena/browser/view/PRJNA12499)
* **Assembly accession**: [GCA_000142805.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000142805.1)
* **Original submitter**: 

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: ASM14280v1
* **Assembly length**: 10,392,862
* **#Chromosomes**: 8
* **Mitochondiral**: No
* **N50 (L50)**: 1,513,537 (4)

### Annotation overview

* **Original annotator**: 
* **CDS count**: 5092
* **Pseudogene count**: 46
* **tRNA count**: 229
* **rRNA count**: 13
* **Mobile element count**: 9
